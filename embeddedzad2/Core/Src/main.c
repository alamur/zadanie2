#include "main.h"
#include "stm32f0xx_hal.h"


I2C_HandleTypeDef hi2c1;

#define PCA9633PWM_ADDR 0x40

void PCA9633PWM_SetColor(uint8_t red, uint8_t green, uint8_t blue, uint8_t white) {

    uint8_t data[5];
    data[0] = 0x00;
    data[1] = red;
    data[2] = green;
    data[3] = blue;
    data[4] = white;

    HAL_I2C_Master_Transmit(&hi2c1, PCA9633PWM_ADDR, data, 5, HAL_MAX_DELAY);
}

int main(void) {
    HAL_Init();
    SystemClock_Config();

    MX_GPIO_Init();
    MX_I2C1_Init();

    while (1) {
        for (int i = 0; i <= 255; i++) {

            uint8_t red, green, blue;

            if (i < 43) {
                red = 255;
                green = 0;
                blue = i * 6;
            } else if (i < 85) {
                red = 255 - (i - 42) * 6;
                green = 0;
                blue = 255;
            } else if (i < 128) {
                red = 0;
                green = (i - 85) * 6;
                blue = 255;
            } else if (i < 170) {
                red = 0;
                green = 255;
                blue = 255 - (i - 127) * 6;
            } else if (i < 213) {
                red = (i - 170) * 6;
                green = 255;
                blue = 0;
            } else if (i < 255) {
                red = 255;
                green = 255 - (i - 212) * 6;
                blue = 0;
            } else {
                red = 255;
                green = 0;
                blue = 0;
            }

            PCA9633PWM_SetColor(red, green, blue, 255);
            HAL_Delay(10);
        }
    }
}
